# Let's get started

> Welcome to DevOps! You are awesome!
---

It's an attempt to make you learn how you can utilise GitLab and its sub-components while provisioning Documentum-Simple-Stack. If you have limited understanding of DevOps, and want to realise how other DevOps engineer thinks, this might be a good start! You'll learn below concepts/principles/method etc.:

1. GitLab Pipelines
2. GitLab Runners
3. GitLab Container Registry
4. Docker Containers
5. Vagrant
6. Ansible
7. Bash

Firstly, you need to have below things.

1. Laptop
   1. RAM : 8 GB+
   2. CPU : 4 Cores+, 1.90 GHz+
2. Operating System (Windows 10/11 Pro)
   1. Hyper-V Enabled
3. Software
   1. Vagrant 2.3.7+
   2. Git Bash
   3. VS Code 1.81.1+

Secondly, you have an account in [gitlab.com](https://gitlab.com) to experience CI/CD parts on this guide. Please follow below plan to learn as you go along.

1. Clone below projects under one workspace in VS Code.
    These vagrant projects can be cosidered as IaaC (Infrastructure as a Code) which only focus on configuring VMs on Hyper-V. These VMs auto belongs to **mshome.net** domain. If you are using AWS/Azure/GCP, you could move these to Terraform later!
   1. [Gitlab-Runner-Vagrant](https://gitlab.com/documentum-stack/vagrant/gitlab-runner-vagrant.git)
      Please follow Readme to setup GitLab Runner for GitLab Pipelines.
   2. [Documentum-Servers-Vagrant](https://gitlab.com/documentum-stack/vagrant/documentum-servers-vagrant.git)
      Please follow Readme to setup Documentum VMs.

2. Once GitLab Runner is ready to be used by Pipelines, please proceed and fork below projects to run pipelines.
   1. [Containers/Ansible](https://gitlab.com/documentum-stack/containers/ansible.git)
      Please follow Readme to build and published ansible image under GitLab Container Registry.
   2. TBC
